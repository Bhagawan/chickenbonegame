package com.example.chickenbonegame.util

import android.content.Context
import com.example.chickenbonegame.data.HistoryRecord
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("ChickenBoneGame", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        fun saveBalance(context: Context, balance: Float) = context.getSharedPreferences("ChickenBoneGame", Context.MODE_PRIVATE).edit().putFloat("balance", balance).apply()

        fun getBalance(context: Context): Float  = context.getSharedPreferences("ChickenBoneGame", Context.MODE_PRIVATE).getFloat("balance", 1000.0f).run {
            if(this >= 100) this
            else 1000.0f
        }

        fun saveHistory(context: Context, history: List<HistoryRecord>) = context
            .getSharedPreferences("ChickenBoneGame", Context.MODE_PRIVATE)
            .edit()
            .putString("history", Gson().toJson(history))
            .apply()

        fun getHistory(context: Context): List<HistoryRecord> {
            val h = context.getSharedPreferences("ChickenBoneGame", Context.MODE_PRIVATE)
                .getString("history", "[]")

            return Gson().fromJson(h, Array<HistoryRecord>::class.java).toList()
        }


        private inline fun <reified T> Gson.fromJson(json: String): T = fromJson(json, object: TypeToken<T>() {}.type)
    }
}