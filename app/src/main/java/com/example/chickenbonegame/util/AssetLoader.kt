package com.example.chickenbonegame.util

import android.content.Context
import android.graphics.Bitmap
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.example.chickenbonegame.R
import com.example.chickenbonegame.data.static.Assets
import com.example.chickenbonegame.data.static.UrlAssetChicken
import com.example.chickenbonegame.data.static.UrlAssetLid
import com.example.chickenbonegame.data.static.UrlAssetPlate
import com.example.chickenbonegame.util.api.ImageDownloader
import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.EmptyCoroutineContext

class AssetLoader(private val context: Context) {
    private var mInterface: AssetLoaderInterface? = null
    private val scope = CoroutineScope(EmptyCoroutineContext)

    fun setInterface(i: AssetLoaderInterface) {
        mInterface = i
    }

    fun loadAssets() {
        Assets.boneBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_bone)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
        ImageDownloader.urlToBitmap(context, scope, UrlAssetChicken, onSuccess = {
            Assets.chickenBitmap = it
            checkCompletion()
        }, onError = {
            mInterface?.onError()
        })
        ImageDownloader.urlToBitmap(context, scope, UrlAssetPlate, onSuccess = {
            Assets.plateBitmap = it
            checkCompletion()
        }, onError = {
            mInterface?.onError()
        })
        ImageDownloader.urlToBitmap(context, scope, UrlAssetLid, onSuccess = {
            Assets.lidBitmap = it
            checkCompletion()
        }, onError = {
            mInterface?.onError()
        })
    }

    private fun checkCompletion() {
        if(Assets.loaded) mInterface?.onAssetsLoaded()
    }


    interface AssetLoaderInterface {
        fun onAssetsLoaded()
        fun onError()
    }
}