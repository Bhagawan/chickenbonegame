package com.example.chickenbonegame.ui.composables

import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.example.chickenbonegame.data.HistoryRecord
import com.example.chickenbonegame.ui.theme.Green_light
import com.example.chickenbonegame.ui.theme.Grey_light
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun HistoryItem(record: HistoryRecord, modifier: Modifier = Modifier) {
    val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    val time = dateFormat.format(record.time)
    val coeff = if(record.win) String.format("%.2f", record.coefficient) else "-"
    val win = if(record.win) String.format("%.2f", record.coefficient * record.bet) else "-"
    val color = if(record.win) Green_light else Grey_light
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically) {
        Text(
            time,
            textAlign = TextAlign.Center,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = color,
            modifier = Modifier.weight(1.0f, true))
        Text(
            record.bet.toString(),
            textAlign = TextAlign.Center,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = color,
            modifier = Modifier.weight(1.0f, true))
        Text(
            coeff,
            textAlign = TextAlign.Center,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = color,
            modifier = Modifier.weight(1.0f, true))
        Text(
            win,
            textAlign = TextAlign.Center,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = color,
            modifier = Modifier.weight(1.0f, true))
    }
}