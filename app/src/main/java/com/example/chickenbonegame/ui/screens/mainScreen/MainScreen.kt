package com.example.chickenbonegame.ui.screens.mainScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.chickenbonegame.R
import com.example.chickenbonegame.data.static.CurrentAppData
import com.example.chickenbonegame.data.static.UrlAssetChicken
import com.example.chickenbonegame.data.static.UrlBack
import com.example.chickenbonegame.ui.composables.*
import com.example.chickenbonegame.ui.theme.Green_light
import com.example.chickenbonegame.ui.theme.Grey_light
import com.example.chickenbonegame.ui.theme.Transparent_black
import com.example.chickenbonegame.ui.view.ChickenBoneGameView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun MainScreen() {
    var initGame = remember { true }
    val viewModel = viewModel(MainScreenViewModel::class.java)
    val balance by viewModel.balance.collectAsState()
    val bones by viewModel.bones.collectAsState()
    val chickensLeft by viewModel.chickensLeft.collectAsState()
    val gameOn by viewModel.gameOn.collectAsState()
    val bet by viewModel.bet.collectAsState()
    val totalCoefficient by viewModel.totalCoefficient.collectAsState()
    val nextCoefficient by viewModel.nextCoefficient.collectAsState()
    val history by viewModel.history.collectAsState()

    val scrollState = rememberScrollState()

    Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        val height = maxHeight
        val width = maxWidth
        Column(modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState), horizontalAlignment = Alignment.CenterHorizontally) {
            Row(modifier = Modifier
                .width(width / 2)
                .height(30.dp)
                .padding(horizontal = 15.dp)
                .background(Transparent_black, RoundedCornerShape(3.dp))
                .padding(horizontal = 5.dp)) {
                Text(String.format("%.2f", balance), fontSize = 15.sp, color = Green_light, textAlign = TextAlign.Start, modifier = Modifier.weight(1.0f, true))
                Text(stringResource(id = R.string.currency), fontSize = 15.sp, color = Green_light, textAlign = TextAlign.End, modifier = Modifier.weight(1.0f, true))
            }
            Column(modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .height(height * 0.7f - 30.dp)
                .background(Transparent_black), horizontalAlignment = Alignment.CenterHorizontally) {
                AndroidView(factory = { ChickenBoneGameView(it, CurrentAppData.size) },
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1.0f, true),
                    update = { view ->
                        if (initGame) {
                            view.setInterface(object : ChickenBoneGameView.GameInterface {
                                override fun opened(chickenInside: Boolean) {
                                    viewModel.openLid(chickenInside)
                                }
                            })
                            viewModel.gameOn.onEach { if(it) view.start() else view.stop() }.launchIn(viewModel.viewModelScope)
                            viewModel.bones.onEach { view.setBonesAmount(it) }.launchIn(viewModel.viewModelScope)
                            initGame = false
                        }
                    })
                Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                    ImageAmount(amount = bones, painterResource(id = R.drawable.ic_bone))
                    ImageAmount(amount = chickensLeft, UrlAssetChicken)
                }
                Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                    CoefficientCard(header = stringResource(id = R.string.header_next_win), modifier = Modifier.weight(1.0f, true), coefficient = nextCoefficient, total = nextCoefficient * totalCoefficient * bet)
                    CoefficientCard(header = stringResource(id = R.string.header_total_win), modifier = Modifier.weight(1.0f, true), coefficient = nextCoefficient, total = totalCoefficient * bet)
                }
            }
            Row(modifier = Modifier
                .height(height * 0.3f - 20.dp)
                .fillMaxWidth()
                .padding(10.dp)) {
                Column(modifier = Modifier
                    .weight(0.65f, true)
                    .fillMaxHeight()
                    .padding(end = 10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
                    BetPanel(amount = bet, modifier = Modifier.weight(1.0f, true), onIncrease = { viewModel.increaseBetAmount(10) }, onDecrease = { viewModel.increaseBetAmount(-10) })
                    BonesPanel(amount = bones, modifier = Modifier.weight(1.0f, true), onIncrease = { viewModel.increaseBonesAmount(1) }, onDecrease = { viewModel.increaseBonesAmount(-1) })
                }
                
                BetButton(
                    gameOn = gameOn,
                    cashAmount = bet * totalCoefficient,
                    modifier = Modifier
                        .weight(0.35f, true)
                        .fillMaxHeight(),
                    onClick = {
                        if(gameOn) viewModel.cashOut()
                        else viewModel.bet()
                    }
                )
            }

            Text(stringResource(id = R.string.history_header),
                textAlign = TextAlign.Center,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                color = Grey_light,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .background(Transparent_black, RoundedCornerShape(10.dp))
                    .padding(10.dp))
            HistoryTable(history, modifier = Modifier.fillMaxWidth())
        } 
    }
    
    
}