package com.example.chickenbonegame.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.View
import com.example.chickenbonegame.data.Plate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer

class ChickenBoneGameView(context: Context): View(context){

    constructor(context: Context, size: Int): this(context) {
        this.size = size
    }

    private var mWidth = 0
    private var mHeight = 0

    private var size = 5
    private var bones = 1
    private var plates = emptyList<List<Plate>>()

    companion object {
        const val STATE_PAUSE = 0
        const val STATE_GAME = 1
    }

    private var currentState = STATE_PAUSE

    private var mInterface: GameInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            arrangePlates()
        }
    }

    override fun onDraw(canvas: Canvas) {
        drawPlates(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE -> return true

            MotionEvent.ACTION_UP -> {
                if(currentState == STATE_GAME) click(event.x, event.y)
                return true
            }
        }
        return false
    }

    //// Public

    fun setInterface(i: GameInterface) {
        mInterface = i
    }

    fun setSize(s: Int) {
        size = s
        arrangePlates()
    }

    fun setBonesAmount(amount: Int) {
        if(amount != bones) {
            bones = amount
            hideBones()
        }
    }

    fun start() {
        if(currentState == STATE_PAUSE) {
            hideBones()
            currentState =  STATE_GAME
        }
    }

    fun stop() {
        if(currentState == STATE_GAME) currentState = STATE_PAUSE
    }

    //// Private

    private fun drawPlates(c: Canvas) {
        plates.forEach { column -> column.forEach { it.draw(c) } }
    }

    private fun hideBones() {
        plates.forEach {
            column -> column.forEach {
                it.close()
                it.setChicken(true)
            }
        }
        val all = ArrayList<Pair<Int, Int>>().apply {
            for(x in 0 until this@ChickenBoneGameView.size) {
                for(y in 0 until this@ChickenBoneGameView.size) add(x to y)
            }
        }
        var left = bones
        if(all.size > bones) {
            while(left >= 0) {
                val n = all.random()
                try {
                    plates[n.first][n.second].setChicken(false)
                    all.remove(n)
                    left--
                } catch (e: Exception) { }
            }
        }
    }

    private fun arrangePlates() {
        val cellWidth = mWidth / size.toFloat()
        val cellHeight = mHeight / size.toFloat()
        if(plates.size != size) {
            if(plates.size > size) plates = plates.subList(0, size)
            else while(plates.size < size) {
                plates = plates.plus(List(size - plates.size) { List(size) { Plate(0.0f,0.0f,1.0f,1.0f)}})
            }
        }
        for(column in plates.withIndex()) when {
            column.value.size > size -> {
                plates = ArrayList(plates).apply { this[column.index] = this[column.index].take(size) }.toList()
            }
            column.value.size < size -> {
                plates = ArrayList(plates).apply { this[column.index] = this[column.index].plus(List(size - this[column.index].size) { Plate(0.0f,0.0f,1.0f,1.0f)}) }.toList()
            }
        }

        for(x in 0 until size) {
            for(y in 0 until size) {
                try {
                    plates[x][y].setPosition(cellWidth / 2 + cellWidth * x,
                        cellHeight / 2 + cellHeight * y,
                        cellWidth * 0.9f,
                        cellHeight * 0.9f)
                } catch (e: Exception) { }
            }
        }
    }

    private fun click(x: Float, y: Float) {
        val cellWidth = mWidth / size.toFloat()
        val cellHeight = mHeight / size.toFloat()
        val cX = (x / cellWidth).toInt()
        val cY = (y / cellHeight).toInt()

        try {
            if(!plates[cX][cY].isOpen()) {
                plates[cX][cY].open()
                mInterface?.opened(plates[cX][cY].isChicken())
            }
        }
        catch (e: Exception) { }
    }

    interface GameInterface {
        fun opened(chickenInside: Boolean)
    }
}