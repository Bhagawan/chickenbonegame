package com.example.chickenbonegame.ui.screens.mainScreen

import androidx.lifecycle.ViewModel
import com.example.chickenbonegame.data.HistoryRecord
import com.example.chickenbonegame.data.static.CurrentAppData
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.util.*

class MainScreenViewModel: ViewModel() {
    private val _balance = MutableStateFlow(CurrentAppData.balance)
    val balance = _balance.asStateFlow()

    private val _bones = MutableStateFlow(1)
    val bones = _bones.asStateFlow()

    private val _chickensLeft = MutableStateFlow(CurrentAppData.size * CurrentAppData.size - bones.value)
    val chickensLeft = _chickensLeft.asStateFlow()

    private val _bet = MutableStateFlow(100)
    val bet = _bet.asStateFlow()

    private val _totalCoefficient = MutableStateFlow(1.0f)
    val totalCoefficient = _totalCoefficient.asStateFlow()

    private val _nextCoefficient = MutableStateFlow(1.01f)
    val nextCoefficient = _nextCoefficient.asStateFlow()

    private val _gameOn = MutableStateFlow(false)
    val gameOn = _gameOn.asStateFlow()

    private val _history = MutableStateFlow(CurrentAppData.history.toList())
    val history = _history.asStateFlow()

    fun increaseBonesAmount(amount: Int) {
        if(!gameOn.value && amount < chickensLeft.value && (bones.value + amount) >= 1) {
            _bones.tryEmit(bones.value + amount)
            _chickensLeft.tryEmit(chickensLeft.value - amount)
        }
    }

    fun increaseBetAmount(amount: Int) {
        if(!gameOn.value) {
            _chickensLeft.tryEmit(CurrentAppData.size * CurrentAppData.size - bones.value - amount)
            _bet.tryEmit((bet.value + amount).coerceAtLeast(0).coerceAtMost(balance.value.toInt()))
        }
    }

    fun bet() {
        _gameOn.tryEmit(true)
        _balance.tryEmit(balance.value - bet.value)
        CurrentAppData.balance = balance.value
    }

    fun openLid(chickenInside: Boolean) {
        if(chickenInside) {
            _totalCoefficient.tryEmit(totalCoefficient.value * nextCoefficient.value)
            _nextCoefficient.tryEmit(nextCoefficient.value + nextCoefficient.value * 0.01f * bones.value)
            _chickensLeft.tryEmit(chickensLeft.value - 1)
        } else {
            CurrentAppData.history.add(HistoryRecord(Date(), bet.value, totalCoefficient.value, false))
            _history.tryEmit(CurrentAppData.history.toList())
            _totalCoefficient.tryEmit(1.0f)
            _nextCoefficient.tryEmit(1.01f)
            _gameOn.tryEmit(false)
            _chickensLeft.tryEmit(CurrentAppData.size * CurrentAppData.size - bones.value)
            if(balance.value < 100) {
                CurrentAppData.balance = 1000.0f
                _balance.tryEmit(1000.0f)
            }
        }
    }

    fun cashOut() {
        CurrentAppData.history.add(HistoryRecord(Date(), bet.value, totalCoefficient.value, true))
        _history.tryEmit(CurrentAppData.history.toList())
        _balance.tryEmit(balance.value + bet.value * totalCoefficient.value)
        _totalCoefficient.tryEmit(1.0f)
        _nextCoefficient.tryEmit(1.01f)
        _gameOn.tryEmit(false)
        _chickensLeft.tryEmit(CurrentAppData.size * CurrentAppData.size - bones.value)
    }
}