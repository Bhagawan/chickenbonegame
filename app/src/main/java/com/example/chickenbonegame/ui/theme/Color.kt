package com.example.chickenbonegame.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Green_light = Color(0xFF58C50A)
val Transparent_black = Color(0x66000000)
val Transparent_white = Color(0x33FFFFFF)
val Grey_light = Color(0xFFA8A8A8)
val Red = Color(0xFFB41818)
val Yellow = Color(0xFFDFB624)
val Orange = Color(0xFFC56009)