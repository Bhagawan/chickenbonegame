package com.example.chickenbonegame.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.chickenbonegame.ui.theme.Grey_light

@Composable
fun ImageAmount(amount: Int, url: String, modifier: Modifier = Modifier) {
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(5.dp)) {
        Text(amount.toString(), textAlign = TextAlign.Center, fontSize = 15.sp, color = Color.White)
        Text("x", textAlign = TextAlign.Center, fontSize = 15.sp, color = Grey_light)
        Image(rememberImagePainter(url), contentDescription = null, modifier = Modifier.height(20.dp), contentScale = ContentScale.FillHeight)
    }
}

@Composable
fun ImageAmount(amount: Int, image: Painter, modifier: Modifier = Modifier) {
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(5.dp)) {
        Text(amount.toString(), textAlign = TextAlign.Center, fontSize = 15.sp, color = Color.White)
        Text("x", textAlign = TextAlign.Center, fontSize = 15.sp, color = Grey_light)
        Image(image, contentDescription = null, modifier = Modifier.height(20.dp), contentScale = ContentScale.FillHeight)
    }
}