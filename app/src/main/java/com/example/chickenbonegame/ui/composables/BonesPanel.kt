package com.example.chickenbonegame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.chickenbonegame.R
import com.example.chickenbonegame.ui.theme.Green_light
import com.example.chickenbonegame.ui.theme.Grey_light
import com.example.chickenbonegame.ui.theme.Transparent_white

@Composable
fun BonesPanel(amount: Int, modifier: Modifier = Modifier, onIncrease: () -> Unit, onDecrease: () -> Unit) {
    BoxWithConstraints(modifier = modifier) {
        val height = maxHeight
        Row(
            modifier = Modifier
                .fillMaxSize()
                .border(width = 1.dp, color = Grey_light, shape = RoundedCornerShape(3.dp))
                .padding(5.dp), verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                stringResource(id = R.string.bones),
                fontSize = 10.sp,
                color = Grey_light,
                textAlign = TextAlign.Start
            )
            Text(
                amount.toString(),
                fontSize = 15.sp,
                color = Green_light,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .padding(horizontal = 10.dp)
                    .weight(1.0f, true)
            )

            Icon(
                painterResource(id = R.drawable.ic_round_keyboard_arrow_down_24),
                contentDescription = null,
                tint = Grey_light,
                modifier = Modifier
                    .padding(3.dp)
                    .size(height * 0.5f)
                    .aspectRatio(1.0f)
                    .background(color = Transparent_white, CircleShape)
                    .clickable { onDecrease() }
                    .clipToBounds())

            Icon(
                painterResource(id = R.drawable.ic_round_keyboard_arrow_up_24),
                contentDescription = null,
                tint = Grey_light,
                modifier = Modifier
                    .padding(3.dp)
                    .size(height * 0.5f)
                    .aspectRatio(1.0f)
                    .background(color = Transparent_white, CircleShape)
                    .clickable { onIncrease() }
                    .clipToBounds())

        }
    }
}