package com.example.chickenbonegame.data

import androidx.annotation.Keep

@Keep
data class ChickenBoneSplashResponse(val url : String)