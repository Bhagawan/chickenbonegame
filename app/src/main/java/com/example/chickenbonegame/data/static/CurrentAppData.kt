package com.example.chickenbonegame.data.static

import com.example.chickenbonegame.data.HistoryRecord

object CurrentAppData {
    var url = ""
    var balance = 1000.00f
    var history = ArrayList<HistoryRecord>()

    const val size = 5
}