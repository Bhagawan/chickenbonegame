package com.example.chickenbonegame.data.static

const val UrlSplash = "ChickenBoneGame/splash.php"
const val UrlLogo = "http://195.201.125.8/ChickenBoneGame/chicken.png"
const val UrlBack = "http://195.201.125.8/ChickenBoneGame/back.png"
const val UrlAssetChicken = "http://195.201.125.8/ChickenBoneGame/chicken.png"
const val UrlAssetPlate = "http://195.201.125.8/ChickenBoneGame/plate.png"
const val UrlAssetLid = "http://195.201.125.8/ChickenBoneGame/lid.png"