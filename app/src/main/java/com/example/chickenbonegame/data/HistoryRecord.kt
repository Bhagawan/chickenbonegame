package com.example.chickenbonegame.data

import java.util.*

data class HistoryRecord(val time: Date, val bet: Int, val coefficient: Float, val win: Boolean)
