package com.example.chickenbonegame.data.static

import android.graphics.Bitmap

object Assets {
    var chickenBitmap: Bitmap? = null
    var plateBitmap: Bitmap? = null
    var lidBitmap: Bitmap? = null
    var boneBitmap: Bitmap? = null


    val loaded: Boolean
    get() = chickenBitmap != null &&
            plateBitmap != null &&
            lidBitmap != null &&
            boneBitmap != null

}