package com.example.chickenbonegame.data

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import com.example.chickenbonegame.data.static.Assets

class Plate(private var x: Float, private var y: Float, private var width: Float, private var height: Float) {
    private var chickenInside = true
    private var opened = false
    private var openTimer = 30

    private var plateHeight = height * 0.9f

    fun isOpen(): Boolean = opened

    fun open() {
        opened = true
        openTimer = 30
    }

    fun close() {
        opened = false
    }

    fun setPosition(x: Float, y: Float, w: Float = width, h: Float = height) {
        this.x = x
        this.y = y
        width = w
        height = h
        plateHeight = height * 0.05f
    }

    fun setChicken(chickenInside: Boolean = true) {
        this.chickenInside = chickenInside
    }

    fun isChicken() = chickenInside

    fun draw(c: Canvas) {
        val p = Paint()
        Assets.plateBitmap?.let {
            c.drawBitmap(it, null, Rect((x - width / 2.0f).toInt(),
                (y + height / 2.0f - plateHeight).toInt(),
                (x + width / 2.0f).toInt(),
                (y + height / 2.0f).toInt()), p)
        }
        if(!opened) {
            Assets.lidBitmap?.let {
                val w = width * 0.8f
                val h = it.height.toFloat() / it.width * w
                c.drawBitmap(it, null, Rect((x - width / 2.0f).toInt(),
                    (y + height / 2.0f - plateHeight - h).toInt(),
                    (x + width / 2.0f).toInt(),
                    (y + height / 2.0f - plateHeight).toInt()), p)
            }
        } else {
            if(chickenInside) {
                Assets.chickenBitmap?.let {
                    val w = width * 0.5f
                    val h = it.height.toFloat() / it.width * w
                    c.drawBitmap(it, null, Rect((x - w / 2.0f).toInt(),
                        (y + height / 2.0f - plateHeight - h).toInt(),
                        (x + w / 2.0f).toInt(),
                        (y + height / 2.0f - plateHeight).toInt()), p)
                }
            } else {
                Assets.boneBitmap?.let {
                    val w = width * 0.5f
                    val h = it.height.toFloat() / it.width * w
                    c.drawBitmap(it, null, Rect((x - w / 2.0f).toInt(),
                        (y + height / 2.0f - plateHeight - h).toInt(),
                        (x + w / 2.0f).toInt(),
                        (y + height / 2.0f - plateHeight).toInt()), p)
                }
            }

            if(openTimer > 0) {
                openTimer--
                Assets.lidBitmap?.let {
                    val w = width * 0.8f
                    val h = it.height.toFloat() / it.width * w
                    val hD = 30 - openTimer
                    c.drawBitmap(it, null, Rect((x - width / 2.0f).toInt(),
                        (y + height / 2.0f - plateHeight - h - hD).toInt(),
                        (x + width / 2.0f).toInt(),
                        (y + height / 2.0f - plateHeight).toInt() - hD), p)
                }
            }
        }
    }
}